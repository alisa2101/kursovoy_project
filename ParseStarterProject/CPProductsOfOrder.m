//
//  CPProductsOfOrder.m
//  ParseStarterProject
//
//  Created by Alisa_Butskaya on 5/22/15.
//
//

#import "CPProductsOfOrder.h"
#import "CPColor.h"
#import "CPOrder.h"
#import "CPProduct.h"


@implementation CPProductsOfOrder

@dynamic quantity;
@dynamic color;
@dynamic order;
@dynamic product;

@end
