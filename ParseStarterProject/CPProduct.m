//
//  CPProduct.m
//  ParseStarterProject
//
//  Created by Alisa_Butskaya on 5/22/15.
//
//

#import "CPProduct.h"
#import "CPProductsOfOrder.h"
#import "CPType.h"


@implementation CPProduct

@dynamic product;
@dynamic productsOfOrders;
@dynamic type;

@end
