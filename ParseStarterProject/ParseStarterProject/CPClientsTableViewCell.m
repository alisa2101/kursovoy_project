//
//  CPClientsTableViewCell.m
//  ParseStarterProject
//
//  Created by Alisa_Butskaya on 5/25/15.
//
//

#import "CPClientsTableViewCell.h"

@implementation CPClientsTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

-(void)setClient:(CPClient *)client {
    self.firstNameLable.text    = client.firstName;
    self.lastNameLabel.text     = client.lastName;
    self.phoneLabel.text        = client.phone;
    self.adressLabel.text       = client.adress;
}

@end
