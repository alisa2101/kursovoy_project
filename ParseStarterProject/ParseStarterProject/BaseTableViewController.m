//
//  HLPTBaseTableViewController.m
//  helloparts
//
//  Created by "ZinkParts" on 2/24/15.
//  Copyright (c) 2015 "ZinkParts". All rights reserved.
//

#import "BaseTableViewController.h"

@interface BaseTableViewController ()

@end

@implementation BaseTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
}



@end
