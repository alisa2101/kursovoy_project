//
//  ClientsTableViewController.m
//  ParseStarterProject
//
//  Created by Alisa_Butskaya on 5/25/15.
//
//

#import "ClientsTableViewController.h"
#import "CPClientsTableViewCell.h"
#import "CPDataSource.h"
#import "CPClient.h"
#import "CPOrder.h"

@interface ClientsTableViewController ()
@property (strong, nonatomic) NSArray *clients;
@end

@implementation ClientsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Client"];
    
    self.clients = [[CPDataSource defaultDataSource].managedObjectContext executeFetchRequest:fetchRequest error:nil];
    
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return self.clients.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //    добавляем новую запись в табл
    //    CPManager *manager = [NSEntityDescription insertNewObjectForEntityForName:@"Manager" inManagedObjectContext:[CPDataSource defaultDataSource].managedObjectContext];
    
    CPClient *currentClient = [self.clients objectAtIndex:indexPath.row];
    
    CPClientsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"clientCell" forIndexPath:indexPath];
    
    
    cell.firstNameLable.text    = currentClient.firstName;
    cell.lastNameLabel.text     = currentClient.lastName;
    cell.phoneLabel.text        = currentClient.phone;
    cell.adressLabel.text       = currentClient.adress;

    return cell;
}
- (IBAction)touchedAddButton:(id)sender {
    [self performSegueWithIdentifier:@"clientsDetailSegue" sender:self];
}

@end
