//
//  CPManagerTableViewCell.m
//  ParseStarterProject
//
//  Created by Alisa_Butskaya on 5/23/15.
//
//

#import "CPManagerTableViewCell.h"

@implementation CPManagerTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setManager:(CPManager *)manager {
    
    self.firstNameLable.text    = manager.firstName;
    self.secondNameLabel.text   = manager.lastName;
}

@end
