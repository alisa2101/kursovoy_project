//
//  CPDeliveryTableViewCell.h
//  ParseStarterProject
//
//  Created by Alisa_Butskaya on 5/26/15.
//
//

#import <UIKit/UIKit.h>
#import "CPDelivery.h"

@interface CPDeliveryTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *deliveryLabel;

@property (strong, nonatomic) CPDelivery *delivery;
@end
