//
//  MRAGradirntView.h
//  myRoofApp
//
//  Created by Alisa_Butskaya on 5/12/15.
//  Copyright (c) 2015 Alisa_Butskaya. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE @interface MRAGradirntView : UIView

@property (nonatomic) IBInspectable CGFloat cornerRadius;

@property (strong, nonatomic) IBInspectable UIColor *startColor;

@property (strong, nonatomic) IBInspectable UIColor *endColor;


@end
