//
//  ManagersTableViewController.m
//  ParseStarterProject
//
//  Created by Alisa_Butskaya on 5/22/15.
//
//

#import "ManagersTableViewController.h"
#import "CPManagerTableViewCell.h"
#import "CPDataSource.h"
#import "CPManager.h"
#import "CPOrder.h"

@interface ManagersTableViewController ()

@property (strong, nonatomic) NSArray *managers;
@end

@implementation ManagersTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Manager"];
    
    self.managers = [[CPDataSource defaultDataSource].managedObjectContext executeFetchRequest:fetchRequest error:nil];
}

-(void)viewDidAppear:(BOOL)animated {
    [self.tableView reloadData];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return self.managers.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    добавляем новую запись в табл
//    CPManager *manager = [NSEntityDescription insertNewObjectForEntityForName:@"Manager" inManagedObjectContext:[CPDataSource defaultDataSource].managedObjectContext];
    
    CPManager *currentManager = [self.managers objectAtIndex:indexPath.row];
    
    CPManagerTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"managerCell" forIndexPath:indexPath];
    
    cell.firstNameLable.text    = currentManager.firstName;
    cell.secondNameLabel.text   = currentManager.lastName;

    return cell;
}



- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

@end
