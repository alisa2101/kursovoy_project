//
//  ViewController.m
//  myRoofApp
//
//  Created by Alisa_Butskaya on 5/12/15.
//  Copyright (c) 2015 Alisa_Butskaya. All rights reserved.
//

#import "ViewController.h"
#import "MRALoginViewController.h"
#import "MRASettingsViewController.h"

@interface ViewController ()<SettingsViewControllerDelegate>

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    
    
}
- (IBAction)touchedSettingsButton:(id)sender {
    
    MRASettingsViewController *SettingsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"settingsController"];
    SettingsViewController.delegate = self;
    [self performSegueWithIdentifier:@"settingsSegue" sender:self];
    
}

- (void)dataManagerUserDidLogout
{
    MRALoginViewController *loginController = [self.storyboard instantiateViewControllerWithIdentifier:@"loginController"];
    [self.navigationController presentViewController:loginController animated:YES completion:nil];
}

@end
