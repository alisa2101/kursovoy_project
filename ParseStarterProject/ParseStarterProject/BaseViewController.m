//
//  HLPTBaseViewController.m
//  helloparts
//
//  Created by "ZinkParts" on 2/24/15.
//  Copyright (c) 2015 "ZinkParts". All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back.png"] style:UIBarButtonItemStylePlain target:nil action:nil];
}

@end
