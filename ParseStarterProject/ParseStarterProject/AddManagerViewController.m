//
//  AddManagerViewController.m
//  ParseStarterProject
//
//  Created by Alisa_Butskaya on 5/23/15.
//
//

#import "AddManagerViewController.h"
#import "ManagersTableViewController.h"
#import "CPDataSource.h"
#import "CPManager.h"
#import "CPOrder.h"

@interface AddManagerViewController ()

@property(weak, nonatomic) IBOutlet UITextField* firstNameTextField;
@property(weak, nonatomic) IBOutlet UITextField* secondNameTextField;
@end

@implementation AddManagerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions
- (IBAction)saveManagerTouched:(id)sender {
    
    CPManager *manager = [NSEntityDescription insertNewObjectForEntityForName:@"Manager" inManagedObjectContext:[CPDataSource defaultDataSource].managedObjectContext];
    
    
    manager.firstName   = self.firstNameTextField.text;
    manager.lastName    = self.secondNameTextField.text;
    
  //  NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Manager"];
    
  //  NSArray *managers = [[CPDataSource defaultDataSource].managedObjectContext executeFetchRequest:fetchRequest error:nil];
    
    [[CPDataSource defaultDataSource].managedObjectContext save:nil];
    [self.navigationController popViewControllerAnimated:YES];

    
//    CPOrder *order = [NSEntityDescription insertNewObjectForEntityForName:@"Order" inManagedObjectContext:[CPDataSource defaultDataSource].managedObjectContext];
//    
//    CPOrder *order2 = [NSEntityDescription insertNewObjectForEntityForName:@"Order" inManagedObjectContext:[CPDataSource defaultDataSource].managedObjectContext];
//    
//    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"ANY orders = %@",order];
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
