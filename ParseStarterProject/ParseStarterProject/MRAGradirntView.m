//
//  MRAGradirntView.m
//  myRoofApp
//
//  Created by Alisa_Butskaya on 5/12/15.
//  Copyright (c) 2015 Alisa_Butskaya. All rights reserved.
//

#import "MRAGradirntView.h"

@implementation MRAGradirntView

+ (Class)layerClass
{
    return [CAGradientLayer class];
}

#define gradientLayer ((CAGradientLayer *)self.layer)

@synthesize startColor = _startColor;
@synthesize endColor = _endColor;

- (UIColor *)startColor
{
    if (!_startColor) {
        _startColor = [UIColor clearColor];
    }
    return _startColor;
}

- (UIColor *)endColor
{
    if (!_endColor) {
        _endColor = [UIColor clearColor];
    }
    return _endColor;
}

- (void)setStartColor:(UIColor *)startColor
{
    if (startColor == _startColor) {
        return;
    } else if (!startColor) {
        startColor = [UIColor clearColor];
    }
    _startColor = startColor;
    
    gradientLayer.colors = @[ (id)startColor.CGColor, (id)self.endColor.CGColor ];
}

- (void)setEndColor:(UIColor *)endColor
{
    if (endColor == _endColor) {
        return;
    } else if (!endColor) {
        endColor = [UIColor clearColor];
    }
    _endColor = endColor;
    
    gradientLayer.colors = @[ (id)self.startColor.CGColor, (id)endColor.CGColor ];
}

- (CGFloat)cornerRadius
{
    return self.layer.cornerRadius;
}

- (void)setCornerRadius:(CGFloat)cornerRadius
{
    self.layer.cornerRadius = cornerRadius;
}

@end
