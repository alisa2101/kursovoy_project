//
//  AddClientsViewController.h
//  ParseStarterProject
//
//  Created by Alisa_Butskaya on 5/25/15.
//
//

#import <UIKit/UIKit.h>

@interface AddClientsViewController : UIViewController

@property(weak, nonatomic) IBOutlet UITextField *firstNameTextField;
@property(weak, nonatomic) IBOutlet UITextField *lastNameTextField;
@property(weak, nonatomic) IBOutlet UITextField *phoneTextField;
@property(weak, nonatomic) IBOutlet UITextField *adressTextField;

@end
