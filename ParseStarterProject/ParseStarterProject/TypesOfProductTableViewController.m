//
//  TypesOfProductTableViewController.m
//  ParseStarterProject
//
//  Created by Alisa_Butskaya on 5/26/15.
//
//

#import "TypesOfProductTableViewController.h"
#import "TypesofProductTableViewCell.h"
#import "CPDataSource.h"
#import "CPType.h"

@interface TypesOfProductTableViewController ()
@property (strong, nonatomic) NSArray *typesOfProduct;

@end

@implementation TypesOfProductTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Type"];
    
    self.typesOfProduct = [[CPDataSource defaultDataSource].managedObjectContext executeFetchRequest:fetchRequest error:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
        return self.typesOfProduct.count;
}

#pragma mark - Table view deligate

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CPType *currentType = [self.typesOfProduct objectAtIndex:indexPath.row];
    
    TypesofProductTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"typesCell" forIndexPath:indexPath];
    
    cell.typeLabel.text = currentType.type;
    
    return cell;
}

-(IBAction)touchedSaveButton:(id)sender {
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil
                                                       message:@"Add product!"
                                                      delegate:self
                                             cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
    
    alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
    self.textField = [alertView textFieldAtIndex:0];
    [alertView show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        
        CPType *typesOfProduct = [NSEntityDescription insertNewObjectForEntityForName:@"Type" inManagedObjectContext:[CPDataSource defaultDataSource].managedObjectContext];
        
        typesOfProduct.type = self.textField.text;
        
        [[CPDataSource defaultDataSource].managedObjectContext save:nil];
        [self.tableView reloadData];
    }
}


@end
