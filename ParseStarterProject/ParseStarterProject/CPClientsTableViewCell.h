//
//  CPClientsTableViewCell.h
//  ParseStarterProject
//
//  Created by Alisa_Butskaya on 5/25/15.
//
//

#import <UIKit/UIKit.h>
#import "CPClient.h"

@interface CPClientsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *firstNameLable;
@property (weak, nonatomic) IBOutlet UILabel *lastNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property (weak, nonatomic) IBOutlet UILabel *adressLabel;

@property (strong, nonatomic) CPClient *client;
@end
