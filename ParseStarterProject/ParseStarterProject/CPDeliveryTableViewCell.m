//
//  CPDeliveryTableViewCell.m
//  ParseStarterProject
//
//  Created by Alisa_Butskaya on 5/26/15.
//
//

#import "CPDeliveryTableViewCell.h"

@implementation CPDeliveryTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

-(void)setDelivery:(CPDelivery *)delivery {
    self.deliveryLabel.text = delivery.type;
}

@end
