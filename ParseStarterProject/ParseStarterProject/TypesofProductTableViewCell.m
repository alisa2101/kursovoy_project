//
//  TypesofProductTableViewCell.m
//  ParseStarterProject
//
//  Created by Alisa_Butskaya on 5/26/15.
//
//

#import "TypesofProductTableViewCell.h"

@implementation TypesofProductTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

-(void)setType:(CPType *)type {
    self.typeLabel.text = type.type;
}

@end
