//
//  MRASettingsViewController.m
//  myRoofApp
//
//  Created by Alisa_Butskaya on 5/12/15.
//  Copyright (c) 2015 Alisa_Butskaya. All rights reserved.
//

#import "MRASettingsViewController.h"

@interface MRASettingsViewController () <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITableView *settingsView;
@property (weak, nonatomic) IBOutlet UIView *appInfoView;
@property (weak, nonatomic) IBOutlet UITextField *userLoginTextField;
@property (weak, nonatomic) IBOutlet UITextField *userPasswordTextField;
@property (weak, nonatomic) IBOutlet UITextField *userFirstNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *userSecondNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *userDateOfBirthTextField;

@end

@implementation MRASettingsViewController
UIDatePicker *datePicker;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    
    datePicker = [[UIDatePicker alloc] init];
    datePicker.datePickerMode = UIDatePickerModeDate;
    self.userDateOfBirthTextField.inputView = datePicker;
    
    self.userDateOfBirthTextField.delegate = self;
}

-(void)viewWillAppear:(BOOL)animated {
    
//    PFUser *user = [PFUser currentUser];
//    
//    self.userLoginTextField.text       =    user[@"userLogin"];
//    self.userPasswordTextField.text    =    user[@"userPassword"];
//    self.userFirstNameTextField.text   =    user[@"FirstName"];
//    self.userSecondNameTextField.text  =    user[@"SecondName"];
//    self.userDateOfBirthTextField.text =    user[@"DateOfBirth"];

}
#pragma mark - Actions

- (IBAction)touchedSegmentControl:(id)sender {
    UISegmentedControl *segmentedControl = (UISegmentedControl *) sender;
    NSInteger selectedSegment = segmentedControl.selectedSegmentIndex;
    
    if (selectedSegment == 0) {
        //toggle the correct view to be visible
        [self.settingsView setHidden:NO];
        [self.appInfoView setHidden:YES];
    }
    else{
        //toggle the correct view to be visible
        [self.settingsView setHidden:YES];
        [self.appInfoView setHidden:NO];
    }
}
- (IBAction)touchedChangeInfoButton:(id)sender {
    
//    PFUser *user = [PFUser currentUser];
//    [user saveEventually];
//    
//    user[@"userLogin"]      = self.userLoginTextField.text;
//    user[@"userPassword"]   = self.userPasswordTextField.text;
//    user[@"firstName"]      = self.userFirstNameTextField.text;
//    user[@"secondName"]     = self.userSecondNameTextField.text;
//    user[@"dateOfBirth"]    = self.userDateOfBirthTextField.text;
//    
//    [[[UIAlertView alloc] initWithTitle:nil message:@"Your data was saved!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];

}

- (IBAction)touchedLogOutButton:(id)sender {
//    [PFUser logOut];
//    [self.navigationController popToRootViewControllerAnimated:YES];
//    if ([self.delegate respondsToSelector:@selector(dataManagerUserDidLogout)]) {
//        [self.delegate dataManagerUserDidLogout];
//    }
}

#pragma mark - UITableViewDataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 5;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell;
    if (indexPath.row ==0) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    } else if (indexPath.row ==1){
        cell = [tableView dequeueReusableCellWithIdentifier:@"cell2" forIndexPath:indexPath];
    }else if (indexPath.row ==2){
        cell = [tableView dequeueReusableCellWithIdentifier:@"cell3" forIndexPath:indexPath];
    }else if (indexPath.row ==3){
        cell = [tableView dequeueReusableCellWithIdentifier:@"cell4" forIndexPath:indexPath];
    }else if (indexPath.row ==4){
        cell = [tableView dequeueReusableCellWithIdentifier:@"cell5" forIndexPath:indexPath];
    }
    return cell;
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (textField == self.userDateOfBirthTextField) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd/MMM/YYYY"];
        self.userDateOfBirthTextField.text = [dateFormatter stringFromDate:datePicker.date];
    }
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    if (textField == self.userPasswordTextField) {
        [textField resignFirstResponder];
        return NO;
    }
    return YES;
}

#pragma mark - Segue

-(void)performSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
    [self performSegueWithIdentifier:@"detailsSegue" sender:self];
}
@end
