//
//  AddClientsViewController.m
//  ParseStarterProject
//
//  Created by Alisa_Butskaya on 5/25/15.
//
//

#import "AddClientsViewController.h"
#import "CPDataSource.h"
#import "CPClient.h"

@interface AddClientsViewController ()

@end

@implementation AddClientsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions
- (IBAction)saveManagerTouched:(id)sender {
    
    CPClient *client = [NSEntityDescription insertNewObjectForEntityForName:@"Client" inManagedObjectContext:[CPDataSource defaultDataSource].managedObjectContext];
    
    client.firstName    = self.firstNameTextField.text;
    client.lastName     = self.lastNameTextField.text;
    client.phone        = self.phoneTextField.text;
    client.adress       = self.adressTextField.text;
    
    [[CPDataSource defaultDataSource].managedObjectContext save:nil];
    [self.navigationController popViewControllerAnimated:YES];
    
}

@end
