//
//  ProductsTableViewController.h
//  ParseStarterProject
//
//  Created by Alisa_Butskaya on 5/26/15.
//
//

#import <UIKit/UIKit.h>
#import "CPProduct.h"

@interface ProductsTableViewController : UITableViewController
@property (weak, nonatomic) IBOutlet UILabel *productLabel;
@property (weak, nonatomic) IBOutlet UILabel *typeLabel;

@property (strong, nonatomic) CPType *type;
@end
