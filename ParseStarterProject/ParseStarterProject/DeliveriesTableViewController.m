//
//  DeliveriesTableViewController.m
//  ParseStarterProject
//
//  Created by Alisa_Butskaya on 5/26/15.
//
//

#import "DeliveriesTableViewController.h"
#import "CPDeliveryTableViewCell.h"
#import "CPDataSource.h"
#import "CPDelivery.h"

@interface DeliveriesTableViewController ()<UIAlertViewDelegate>
@property (strong, nonatomic) NSArray *deliveries;
@end

@implementation DeliveriesTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Delivery"];
    
    self.deliveries = [[CPDataSource defaultDataSource].managedObjectContext executeFetchRequest:fetchRequest error:nil];
}

-(void)viewDidAppear:(BOOL)animated {
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.deliveries.count;
}

#pragma mark - Table view deligate

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CPDelivery *currentDelivery = [self.deliveries objectAtIndex:indexPath.row];
    
    CPDeliveryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"deliveryCell" forIndexPath:indexPath];
    
    cell.deliveryLabel.text = currentDelivery.type;
    
    return cell;
}

-(IBAction)touchedSaveButton:(id)sender {
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil
                                                       message:@"Add type of delivery!"
                                                      delegate:self
                                             cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
    
    alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
    self.textField = [alertView textFieldAtIndex:0];
    [alertView show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        
        CPDelivery *delivery = [NSEntityDescription insertNewObjectForEntityForName:@"Delivery" inManagedObjectContext:[CPDataSource defaultDataSource].managedObjectContext];
        
        delivery.type = self.textField.text;
        
        [[CPDataSource defaultDataSource].managedObjectContext save:nil];
        [self.tableView reloadData];
    }
}

@end
