//
//  MRALoginViewController.m
//  myRoofApp
//
//  Created by Alisa_Butskaya on 5/12/15.
//  Copyright (c) 2015 Alisa_Butskaya. All rights reserved.
//

#import "MRALoginViewController.h"



@interface MRALoginViewController ()
@property (weak, nonatomic) IBOutlet UITextField *userEmailTextField;
@property (weak, nonatomic) IBOutlet UITextField *userPasswordTextField;

@property (nonatomic, readonly) BOOL isFormDataValid;
@end

@implementation MRALoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)touchedLoginButton:(id)sender {
//    if (!self.isFormDataValid) {
//        return;
//    }
//    
//    PFQuery *query = [PFUser query];
//    NSString *userEmail = self.userEmailTextField.text;
//    NSString *userPassword = self.userPasswordTextField.text;
//    [query whereKey:@"username" equalTo:userEmail];
//    
//    PFQuery *queryPass = [PFUser query];
//    [queryPass whereKey:@"password" equalTo:userPassword];
//    
//    if ([query getFirstObject]) {
//        [self dismissViewControllerAnimated:YES completion:^{
//            [self login];
//        }];
//        
//    } else {
//        [[[UIAlertView alloc] initWithTitle:@"Error!"
//                                    message:@"User with current email or password not found"
//                                   delegate:self
//                          cancelButtonTitle:@"Cancel"
//                          otherButtonTitles:nil, nil] show];
//    }
}

- (void) login {
//    [PFUser logInWithUsernameInBackground:self.userEmailTextField.text password:self.userPasswordTextField.text block:^(PFUser *user, NSError *error) {
//        if (user) {
//            [self dismissViewControllerAnimated:YES completion:nil];
//        }
//    }];
}

- (BOOL)isFormDataValid
{
    BOOL formDataValid = self.userPasswordTextField.text.length>0 && self.userEmailTextField.text.length>0;
    
    if (!formDataValid) {
        [[[UIAlertView alloc] initWithTitle:nil
                                    message:@"Please, fill the gaps"
                                   delegate:self
                          cancelButtonTitle:@"Cancel"
                          otherButtonTitles:nil, nil] show];
    }
    
    return formDataValid;
}

@end
