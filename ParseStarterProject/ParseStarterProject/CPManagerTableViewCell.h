//
//  CPManagerTableViewCell.h
//  ParseStarterProject
//
//  Created by Alisa_Butskaya on 5/23/15.
//
//

#import <UIKit/UIKit.h>
#import "CPManager.h"

@interface CPManagerTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *firstNameLable;
@property (weak, nonatomic) IBOutlet UILabel *secondNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateOfBirthLabel;

@property (strong, nonatomic) CPManager *manager;
@end
