//
//  TypesofProductTableViewCell.h
//  ParseStarterProject
//
//  Created by Alisa_Butskaya on 5/26/15.
//
//

#import <UIKit/UIKit.h>
#import "CPType.h"

@interface TypesofProductTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *typeLabel;

@property (strong, nonatomic) CPType *type;
@end
