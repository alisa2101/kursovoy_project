//
//  MRASettingsViewController.h
//  myRoofApp
//
//  Created by Alisa_Butskaya on 5/12/15.
//  Copyright (c) 2015 Alisa_Butskaya. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SettingsViewControllerDelegate;


@interface MRASettingsViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) id<SettingsViewControllerDelegate> delegate;

@end
@protocol SettingsViewControllerDelegate <NSObject>

- (void)dataManagerUserDidLogout;

@end