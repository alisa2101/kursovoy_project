//
//  CPAppDelegate.h
//  ParseStarterProject
//
//  Created by Alisa_Butskaya on 5/23/15.
//
//

#import <UIKit/UIKit.h>

@interface CPAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end