//
//  CPColor.h
//  ParseStarterProject
//
//  Created by Alisa_Butskaya on 5/22/15.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class CPProductsOfOrder;

@interface CPColor : NSManagedObject

@property (nonatomic, retain) NSString * color;
@property (nonatomic, retain) NSSet *productsOfOrders;
@end

@interface CPColor (CoreDataGeneratedAccessors)

- (void)addProductsOfOrdersObject:(CPProductsOfOrder *)value;
- (void)removeProductsOfOrdersObject:(CPProductsOfOrder *)value;
- (void)addProductsOfOrders:(NSSet *)values;
- (void)removeProductsOfOrders:(NSSet *)values;

@end
