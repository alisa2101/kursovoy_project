//
//  CPManager.h
//  ParseStarterProject
//
//  Created by Alisa_Butskaya on 5/22/15.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class CPOrder;

@interface CPManager : NSManagedObject

@property (nonatomic, retain) NSString * firstName;
@property (nonatomic, retain) NSString * lastName;
@property (nonatomic, retain) NSSet *orders;
@end

@interface CPManager (CoreDataGeneratedAccessors)

- (void)addOrdersObject:(CPOrder *)value;
- (void)removeOrdersObject:(CPOrder *)value;
- (void)addOrders:(NSSet *)values;
- (void)removeOrders:(NSSet *)values;

@end
