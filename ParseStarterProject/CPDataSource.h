//
//  CPDataSource.h
//  ParseStarterProject
//
//  Created by Alisa_Butskaya on 5/22/15.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface CPDataSource : NSObject

+ (instancetype)defaultDataSource;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;

@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@end
