//
//  CPColor.m
//  ParseStarterProject
//
//  Created by Alisa_Butskaya on 5/22/15.
//
//

#import "CPColor.h"
#import "CPProductsOfOrder.h"


@implementation CPColor

@dynamic color;
@dynamic productsOfOrders;

@end
