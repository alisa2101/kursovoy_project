//
//  CPType.m
//  ParseStarterProject
//
//  Created by Alisa_Butskaya on 5/22/15.
//
//

#import "CPType.h"
#import "CPProduct.h"


@implementation CPType

@dynamic type;
@dynamic products;

@end
