//
//  CPType.h
//  ParseStarterProject
//
//  Created by Alisa_Butskaya on 5/22/15.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class CPProduct;

@interface CPType : NSManagedObject

@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSSet *products;
@end

@interface CPType (CoreDataGeneratedAccessors)

- (void)addProductsObject:(CPProduct *)value;
- (void)removeProductsObject:(CPProduct *)value;
- (void)addProducts:(NSSet *)values;
- (void)removeProducts:(NSSet *)values;

@end
