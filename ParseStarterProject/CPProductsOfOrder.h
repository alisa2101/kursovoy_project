//
//  CPProductsOfOrder.h
//  ParseStarterProject
//
//  Created by Alisa_Butskaya on 5/22/15.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class CPColor, CPOrder, CPProduct;

@interface CPProductsOfOrder : NSManagedObject

@property (nonatomic, retain) NSNumber * quantity;
@property (nonatomic, retain) CPColor *color;
@property (nonatomic, retain) CPOrder *order;
@property (nonatomic, retain) CPProduct *product;

@end
