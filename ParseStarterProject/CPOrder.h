//
//  CPOrder.h
//  ParseStarterProject
//
//  Created by Alisa_Butskaya on 5/22/15.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class CPClient, CPDelivery, CPManager, CPProductsOfOrder;

@interface CPOrder : NSManagedObject

@property (nonatomic, retain) NSDate * dateOfDelivery;
@property (nonatomic, retain) NSDate * dateOfOrder;
@property (nonatomic, retain) CPClient *client;
@property (nonatomic, retain) CPDelivery *delivery;
@property (nonatomic, retain) CPManager *manager;
@property (nonatomic, retain) NSSet *productsOfOrders;
@end

@interface CPOrder (CoreDataGeneratedAccessors)

- (void)addProductsOfOrdersObject:(CPProductsOfOrder *)value;
- (void)removeProductsOfOrdersObject:(CPProductsOfOrder *)value;
- (void)addProductsOfOrders:(NSSet *)values;
- (void)removeProductsOfOrders:(NSSet *)values;

@end
