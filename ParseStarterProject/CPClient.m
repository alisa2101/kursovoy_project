//
//  CPClient.m
//  ParseStarterProject
//
//  Created by Alisa_Butskaya on 5/22/15.
//
//

#import "CPClient.h"
#import "CPOrder.h"


@implementation CPClient

@dynamic adress;
@dynamic firstName;
@dynamic lastName;
@dynamic phone;
@dynamic orders;

@end
