//
//  CPDelivery.h
//  ParseStarterProject
//
//  Created by Alisa_Butskaya on 5/22/15.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class CPOrder;

@interface CPDelivery : NSManagedObject

@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSSet *orders;
@end

@interface CPDelivery (CoreDataGeneratedAccessors)

- (void)addOrdersObject:(CPOrder *)value;
- (void)removeOrdersObject:(CPOrder *)value;
- (void)addOrders:(NSSet *)values;
- (void)removeOrders:(NSSet *)values;

@end
