//
//  CPDataSource.m
//  ParseStarterProject
//
//  Created by Alisa_Butskaya on 5/22/15.
//
//

static NSString *const modelName = @"Model";
static NSString *const storeFile = @"Model.sqlite";

#import "CPDataSource.h"


@implementation CPDataSource

+ (instancetype)defaultDataSource
{
    static id __defaultDataSource = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __defaultDataSource = [[[self class] alloc] init];
        
        NSURL *documentsURL = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
        NSURL *modelURL = [[NSBundle mainBundle] URLForResource:modelName withExtension:@"momd"];
        NSURL *storeURL = [documentsURL URLByAppendingPathComponent:storeFile];
        
        [__defaultDataSource setupCoreDataStackWithType:NSSQLiteStoreType modelURL:modelURL storeURL:storeURL migrationEnabled:YES];
    });
    return __defaultDataSource;
}

- (void)setupCoreDataStackWithType:(NSString *)storeType modelURL:(NSURL *)modelURL storeURL:(NSURL *)storeURL
{
    [self setupCoreDataStackWithType:storeType modelURL:modelURL storeURL:storeURL migrationEnabled:YES];
}

- (void)setupCoreDataStackWithType:(NSString *)storeType modelURL:(NSURL *)modelURL storeURL:(NSURL *)storeURL migrationEnabled:(BOOL)migrationEnabled
{
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:_managedObjectModel];
    
    NSError *error = nil;
    NSPersistentStore *store = [_persistentStoreCoordinator addPersistentStoreWithType:storeType configuration:nil URL:storeURL options:nil error:&error];
    if (!store) {
        if (migrationEnabled) {
            // perform automatic lightweight migration
            NSDictionary *options = @{ NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES };
            store = [_persistentStoreCoordinator addPersistentStoreWithType:storeType configuration:nil URL:storeURL options:options error:&error];
            if (!store) {
                NSLog(@"Error: %@, %@", error.localizedDescription, error.userInfo);
                abort();
            }
        } else {
            NSLog(@"Error: %@, %@", error.localizedDescription, error.userInfo);
            abort();
        }
    }
    
    _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    _managedObjectContext.persistentStoreCoordinator = _persistentStoreCoordinator;
}

@end
