//
//  CPOrder.m
//  ParseStarterProject
//
//  Created by Alisa_Butskaya on 5/22/15.
//
//

#import "CPOrder.h"
#import "CPClient.h"
#import "CPDelivery.h"
#import "CPManager.h"
#import "CPProductsOfOrder.h"


@implementation CPOrder

@dynamic dateOfDelivery;
@dynamic dateOfOrder;
@dynamic client;
@dynamic delivery;
@dynamic manager;
@dynamic productsOfOrders;

@end
